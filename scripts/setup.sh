#!/bin/bash

# exit if a command returns a non-zero exit code and also print the commands and their args as they are executed
set -e -x

# pulumi
export PATH=$PATH:$HOME/.pulumi/bin
# Login into pulumi. This will require the PULUMI_ACCESS_TOKEN environment variable
pulumi login

#helm
helm repo add datawire https://www.getambassador.io
helm repo add jetstack https://charts.jetstack.io

# doctl

doctl auth init -t "$DIGITALOCEAN_TOKEN"
doctl kubernetes cluster kubeconfig save demos-74601c3
