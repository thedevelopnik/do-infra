import * as pulumi from "@pulumi/pulumi";
import * as digitalocean from "@pulumi/digitalocean";
import * as k8s from "@pulumi/kubernetes";
import * as kx from "@pulumi/kubernetesx";
const config = new pulumi.Config();
config.require("doToken");

const defaultTTL = 3600

const demosVPC = new digitalocean.Vpc("demos", {
  region: "nyc1",
  ipRange: "10.32.0.0/18",
})

const domain = new digitalocean.Domain("thedevelopnik.com", {
  name: "thedevelopnik.com",
})

const blogRecord = new digitalocean.DnsRecord("www", {
  domain: domain.name,
  name: "www",
  type: "CNAME",
  value: "thedevelopnik.gitlab.io.",
  ttl: defaultTTL,
})

const blogConfRecord = new digitalocean.DnsRecord("_gitlab-pages-verification-code.www", {
  domain: domain.name,
  name: "_gitlab-pages-verification-code.www",
  type: "TXT",
  value: "gitlab-pages-verification-code=41552356eafd231e8dcc1ca7732f6503",
  ttl: defaultTTL,
})

const demosRecord = new digitalocean.DnsRecord("apps", {
  domain: domain.name,
  name: "apps",
  type: "A",
  value: "138.197.229.70",
  ttl: 30,
})

const demosCluster = new digitalocean.KubernetesCluster("demos", {
  nodePool: {
    name: "general",
    autoScale: true,
    maxNodes: 5,
    minNodes: 1,
    labels: {
      "thedevelopnik.com/usage": "general",
    },
    size: "s-2vcpu-4gb",
  },
  region: "nyc1",
  version: "1.17.5-do.0",
  vpcUuid: demosVPC.id,
});

const ambassadorNS = new k8s.core.v1.Namespace("ambassador", {
  metadata: {
    name: "ambassador"
  }
});

const ambassadorEdgeStack = new k8s.helm.v3.Chart("ambassador", {
  repo: "datawire",
  chart: "ambassador",
  namespace: ambassadorNS.metadata.name,
  version: "6.3.6",
  values: {
    podAnnotations: {
      "linkerd.io/inject": "enabled",
    },
    env: {
      POLL_EVERY_SECS: "86400"
    }
  }
});

const certManagerNS = new k8s.core.v1.Namespace("cert-manager", {
  metadata: {
    name: "cert-manager"
  }
})

const certManager = new k8s.helm.v3.Chart("cert-manager", {
  repo: "jetstack",
  chart: "cert-manager",
  namespace: certManagerNS.metadata.name,
  version: "v0.15.0",
  values: {
    installCRDs: true
  }
});

const doTokenSecret = new kx.Secret("do-token", {
  metadata: {
    name: "do-token",
    namespace: certManagerNS.metadata.name
  },
  stringData: {
    "access-token": config.getSecret("doToken") || "bogus"
  }
});

const issuer = new k8s.yaml.ConfigFile("thedevelopnik-issuer", {
  file: "issuers/thedevelopnik.yaml"
});

const ambassadorTLS = new k8s.yaml.ConfigFile("thedevelopnik-tls", {
  file: "ambassador/tls.yaml"
});
